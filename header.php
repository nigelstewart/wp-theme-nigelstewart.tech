<?php

?><!doctype html>

<html lang="en">
<head>
  <meta charset="utf-8">

  <title>Nigel Stewart</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="Web Development, Music, Blog">
  <meta name="author" content="Nigel Stewart">

  <?php wp_head() ?>

  <!--[if lt IE 9]>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script>
  <![endif]-->
</head>

<body>
  <div class="row flex-main">
    <div class="col-sm-2 meta">
      <a href="/">
        <img src="<?php bloginfo('template_url'); ?>/images/logo.svg"/>
      </a>
      <h1>Archive</h1>
      <ul>
        <?php wp_get_archives() ?>
      </ul>

      <h1>Categories</h1>
      <ul>
        <?php wp_list_categories( array('title_li' => '') ) ?>
      </ul>

    </div>

    <div class="col-sm-9">
      <nav>
        <div class="search row">
          <?php get_search_form(); ?>
        </div>

        <div id="menu-trigger"><i class="fa fa-caret-down" aria-hidden="true"></i> Menu</div>

        <ul id="menu">
          <li><a href="/work">WORK</a></li>
          <li><a href="/school">SCHOOL</a></li>
          <li><a href="/music">MUSIC</a></li>
          <li><a href="/blog">BLOG</a></li>
        </ul>
      </nav>
