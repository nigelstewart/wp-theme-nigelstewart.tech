<?php get_header();
$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
  $args = array(
    'post_type' => 'post',
    'category__not_in' => array(2,5,4),
    'posts_per_page' => 6,
    'paged' => $paged
  );
  $query = new WP_Query( $args );
  $temp_query = $wp_query;
  $wp_query   = NULL;
  $wp_query   = $query;
?>
<main>
  <div id="content" class="container-fluid">
    <h1><?php single_cat_title() ?></h1>

    <div class="row">
      <?php if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>
        <div class="col-xs-12 col-sm-4">
          <a href="<?php the_permalink() ?>">
            <div class="col-sm-12 article">
              <?php if ( has_post_thumbnail() ) : ?>
                <img src="<?php the_post_thumbnail_url() ?>" />
              <?php endif; ?>
              <h2>
                <?php if ( in_category('Music') ) : ?>
                  <i class="fa fa-music" aria-hidden="true"></i>
                <?php elseif (in_category('School') ) : ?>
                  <i class="fa fa-graduation-cap" aria-hidden="true"></i>
                <?php elseif (in_category('Work') ) : ?>
                  <i class="fa fa-briefcase" aria-hidden="true"></i>
                <?php else : ?>
                  <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                <?php endif; ?>
                <?php the_title() ?>
              </h2>
              <p><?php the_excerpt() ?></p>
            </div>
          </a>
        </div>
      <?php endwhile; ?>
        <div id="pagination" class="col-xs-12">
          <?= paginate_links(); ?>
        </div>
      <?php $wp_query = NULL; $wp_query = $temp_query; endif; ?>
    </div>
  </div>
</main>
<?php get_footer();
