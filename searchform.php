<form role="search" method="get" class="searchform group" action="<?php echo home_url( '/' ); ?>">
  <div class="col-xs-10 col-sm-4">
    <span class="fa fa-search"></span>
    <input placeholder="Search" type="search" class="search-field" value="<?php echo get_search_query() ?>" name="s"
    title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" />
  </div>
</form>
