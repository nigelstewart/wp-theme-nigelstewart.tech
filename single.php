<?php get_header(); ?>
<main>
  <div id="content" class="container">
    <?php while ( have_posts() ) : the_post(); ?>
      <!-- <img id="featured-image" src="<?php the_post_thumbnail_url() ?>" /> -->
      <div class="parallax-window"></div>
      <h1><?php the_title(); ?></h1>
      <?php the_date('d-m-Y', '<h2 id="post-date">', '</h2>'); ?>
      <div><?php the_content(); ?></div>
    <?php endwhile; ?>
  </div>
</main>

<script>
  jQuery('.parallax-window').parallax({imageSrc: '<?php the_post_thumbnail_url() ?>'});
</script>
<?php get_footer();
