<?php

add_theme_support( 'post-thumbnails' );

function ns_enqueue_style() {
  wp_enqueue_style( 'style', get_stylesheet_uri() );
}
add_action( 'wp_enqueue_scripts', 'ns_enqueue_style' );

function ns_enqueue_grid() {
  wp_enqueue_style( 'grid12', get_template_directory_uri() . '/grid12.css' );
}
add_action( 'wp_enqueue_scripts', 'ns_enqueue_grid', 'style' );

function add_google_fonts() {
  wp_enqueue_style('google-fonts', 'https://fonts.googleapis.com/css?family=Raleway:400,700', false);
}
add_action( 'wp_enqueue_scripts', 'add_google_fonts');

function add_font_awesome() {
  wp_enqueue_style('font-awesome', get_template_directory_uri() . '/lib/font-awesome-4.7.0/css/font-awesome.min.css');
}
add_action('wp_enqueue_scripts', 'add_font_awesome');

function mobile_menu_trigger() {
  wp_enqueue_script('mobile-menu', get_template_directory_uri() . '/js/mobile-menu.js', array( 'jquery' ) );
}
add_action('wp_enqueue_scripts', 'mobile_menu_trigger');

function parallax_scrolling() {
  wp_enqueue_script('parallax-scrolling', get_template_directory_uri() . '/lib/parallax.js-1.4.2/parallax.min.js', array( 'jquery' ) );
}
add_action('wp_enqueue_scripts', 'parallax_scrolling');

?>
