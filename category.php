<?php get_header(); ?>
<main>
  <div id="content" class="container-fluid">
    <h1><?php single_cat_title() ?></h1>

    <div class="row">
      <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <div class="col-xs-12 col-sm-4">
          <a href="<?php the_permalink() ?>">
            <div class="col-sm-12 article">
              <?php if ( has_post_thumbnail() ) : ?>
                <img src="<?php the_post_thumbnail_url() ?>" />
              <?php endif; ?>
              <h2>
                <?php if ( in_category('Music') ) : ?>
                  <i class="fa fa-music" aria-hidden="true"></i>
                <?php elseif (in_category('School') ) : ?>
                  <i class="fa fa-graduation-cap" aria-hidden="true"></i>
                <?php elseif (in_category('Work') ) : ?>
                  <i class="fa fa-briefcase" aria-hidden="true"></i>
                <?php else : ?>
                  <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                <?php endif; ?>
                <?php the_title() ?>
              </h2>
              <p><?php the_excerpt() ?></p>
            </div>
          </a>
        </div>
      <?php endwhile; ?>
        <div id="pagination" class="col-xs-12">
          <?= paginate_links( $args ); ?>
        </div>
      <?php endif; ?>
    </div>
  </div>
</main>
<?php get_footer();
