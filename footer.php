</div>
</div>
<footer>
  <div class="logo-container">
    <a href="/" id="mobile-logo">
      <img src="<?php bloginfo('template_url'); ?>/images/logo.svg" width="100"/>
    </a>
  </div>

  <div class="row">
    <div class="col-sm-12 col-md-4 about">
      <h2>About</h2>
      <p>I'm a web developer, musician, and technology enthusiast from Winnipeg, Manitoba.</p>
    </div>

    <div class="col-sm-12 col-md-8 connect">
      <h2>Connect</h2>
      <ul>
        <li><a href="https://ca.linkedin.com/in/nigel-stewart-1a38a5126"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
        <li><a href="https://github.com/nigel-stewart"><i class="fa fa-github-alt" aria-hidden="true"></i></a></li>
        <li><a href="https://soundcloud.com/kaiku"><i class="fa fa-soundcloud" aria-hidden="true"></i></a></li>
        <li><a href="https://plus.google.com/u/0/102114947749431277607"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
        <li><a href="mailto:contact@nigelstewart.tech"><i class="fa fa-envelope-o" aria-hidden="true"></i></a></li>
      </ul>
    </div>
  </div>

  <span>© 2017 Nigel Stewart</span>
</footer>
<?php wp_footer(); ?>
</body>
</html>
